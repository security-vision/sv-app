import logging, os, threading, json
from . import data
from datetime import datetime

logger = logging.getLogger("utils")


def integrate_coords(properties, cities, countries):
    """Integrate missing coordinates looking up to a dataset of cities"""
    if "coords" not in properties and "city" in properties:
        logger.warning(
            f'Missing coordinates info for "{properties["city"]}", trying to integrate from cities list'
        )
        city = properties["city"]
        if city in cities and "coords" in cities[city]:
            properties["coords"] = cities[city]["coords"]
            logger.debug("Found coordinates!")
        else:
            logger.warning("No coordinates available")
    if "coords" not in properties and "country" in properties:
        logger.warning(
            f'Missing coordinates info for "{properties["country"]}". Trying to integrate from countries list.'
        )
        country = properties["country"]
        if country in countries and "coords" in countries[country]:
            properties["coords"] = countries[country]["coords"]
            logger.debug("Found coordinates!")
        else:
            logger.warning("No coordinates available")
    return properties


def last_line(file: str):
    with open(file, "rb") as f:
        try:  # catch OSError in case of a one line file
            f.seek(-2, os.SEEK_END)
            while f.read(1) != b"\n":
                f.seek(-2, os.SEEK_CUR)
        except OSError:
            f.seek(0)
        return f.readline().decode().rstrip()


class DataThread(threading.Thread):
    def __init__(self, output, test=False):
        super(DataThread, self).__init__()
        self.output = output
        self.test = test

    def log(self, status, **kwargs):
        return {"status": status, "timestamp": datetime.now().isoformat(), **kwargs}

    def run(self):
        logfile = os.path.join(self.output, ".updates")

        with open(logfile, "a") as f:
            f.write(json.dumps(self.log("BUILDING")) + "\n")

        data.get_data(output=self.output, TEST=self.test)

        with open(logfile, "a") as f:
            f.write(json.dumps(self.log("UPDATED")) + "\n")


class PropertiesThread(threading.Thread):
    def __init__(self, output, category, test=False):
        super(PropertiesThread, self).__init__()
        self.output = output
        self.test = test
        self.category = category

    def log(self, status, **kwargs):
        return {"status": status, "timestamp": datetime.now().isoformat(), **kwargs}

    def run(self):
        propfile = os.path.join(self.output, f"{self.category}.props.json")

        with open(propfile, "w") as f:
            f.write(json.dumps(self.log("BUILDING")))

        properties = data.list_properties(category=self.category, TEST=self.test)

        with open(propfile, "w") as f:
            f.write(json.dumps(properties))
