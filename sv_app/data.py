import json
import logging
import os
import uuid
from . import queries
from . import session
from . import utils
from . import graph


def get_data(
    categories=[
        # "Institution",
        # "Dataset",
        # "Products",
        "Deployments",
        "Deployment_Type",
    ],
    output="",
    TEST=False,
):
    """Build dataset with a list of nodes and links from a list of categories."""

    logging.basicConfig(
        filename=os.path.join(output, ".log"), encoding="utf-8", level=logging.INFO
    )
    logger = logging.getLogger()

    # Global variables
    base_url = "https://www.securityvision.io/wiki/api.php"
    SESSION = session.init(base_url)

    collection = {"nodes": [], "links": []}

    nodes = {}
    links = []

    cities, city_missing_coords = queries.get_cities(base_url, SESSION)
    countries, country_missing_coords = queries.get_countries(base_url, SESSION)

    with open(
        os.path.join(output, "cities_missing_coords.json"), "w", encoding="utf-8"
    ) as f:
        logger.info("Writing list of incomplete city entries")
        json.dump(city_missing_coords, f)

    with open(
        os.path.join(output, "countries_missing_coords.json"), "w", encoding="utf-8"
    ) as f:
        logger.info("Writing list of incomplete country entries")
        json.dump(country_missing_coords, f)

    for category in categories:
        pages = queries.get_pages_for_category(category, base_url, SESSION, TEST)
        for page in pages:
            name = page["title"]
            if name not in nodes:
                id = page["pageid"]
            else:
                id = nodes[name]["id"]

            node = {"id": id, "name": name, "category": category}

            if name in nodes:
                node = {**node, **nodes[name]}

            nodes[name] = node

            data = queries.get_data_for_page(page, base_url, SESSION)
            properties = parse_data(data)
            properties = utils.integrate_coords(properties, cities, countries)

            for key, value in properties.items():

                def add_link(key, id, name, active=False):
                    """Check if link entity is already in nodes, otherwise create it"""
                    if name not in nodes:
                        logger = logging.getLogger("links")
                        node_id = uuid.uuid4().time_low
                        logger.warning(
                            f'"{name}" doesn\'t extist. Generting node with id {node_id}'
                        )
                        nodes[name] = {
                            "id": node_id,
                            "name": name,
                        }
                    links.append(
                        {
                            "edge_type": key,
                            "from": nodes[name]["id"] if active else id,
                            "to": id if active else nodes[name]["id"],
                        }
                    )

                # Check if link
                # there are two categories of link: `active` and `passive`
                # `active` are for example `dataset_used`, `software_deployed`, ... - ex: item deploys software id
                # `passive` are for example `managed_by`, `used_by`, `developed_by` - ex: id is managed by item

                if key in [
                    "managed_by",
                    "used_by",
                    "developed_by",
                    "owning_institution",
                    "is_department_of",
                    "custodian_institution",
                    "related_institutions",
                    "dataset_used",
                    "software_deployed",
                    "deployment_type",
                ]:
                    active = key in [
                        "related_institutions",
                        "dataset_used",
                        "software_deployed",
                    ]

                    if type(value) == list:
                        for item in value:
                            add_link(key, id, item, active)
                    else:
                        add_link(key, id, value, active)
                else:
                    # else push to node properties
                    node[key] = value

            nodes[name] = node

    collection["nodes"] = list(nodes.values())
    collection["links"] = links

    collection["nodes"] = graph.compute_betweenness(
        collection["nodes"], collection["links"]
    )

    communities = graph.louvain_communities(collection["nodes"], collection["links"])
    collection["nodes"] = graph.add_community_to_nodes(collection["nodes"], communities)

    meta = {
        "communities": {
            index: list(community)
            for index, community in enumerate(communities)
            if len(community) > 1
        },
    }

    collection["meta"] = meta

    # collection["nodes"] = graph.compute_eigenvector(
    #     collection["nodes"], collection["links"]
    # )

    with open(os.path.join(output, "data.json"), "w", encoding="utf-8") as f:
        logger.info("Writing to file export/data.json")
        json.dump(collection, f)


def parse_data(data):
    properties = {}
    if data["query"]["data"]:
        # print()
        for el in data["query"]["data"]:
            # transform property name to lowercase
            key = el["property"].lower()
            # print(f'{key}:  {el["dataitem"]}')

            # Filter out irrelevant properties
            if key in [
                # "_ask",
                "_errc",
                "_inst",
                "_mdat",
                "_skey",
                "_sobj",
                "__sci_cite_reference",
                "cited_references",
                "creation_date",
                "deployment_status",
                "excluded_from_graph",
                "fullname",
                "full_name",
                "has_full_name",
                "has_event",
                "importance",
                "is_in_greens_report_2021",
                "keywods",
                "keywords",
                "needs_processing_of_the_title",
                # "type_of_juris",
                # "links",
                # "address",
                # "information_certainty",
                # "datasets_used",
                # "involved_entities",
            ]:
                continue

            # Consider only following properties
            # if key not in [
            #     'title',
            #     'country',
            #     'city',
            #     'geolocation',
            #     '_uri'
            # ]:
            #     continue

            # Parse values
            name, values = parse_property(key, el["dataitem"])
            properties[name] = values
    return properties


def parse_property(key, values):
    """Extract and clean property name and values"""
    values = [x["item"].rsplit("#0##", 1)[0].replace("_", " ") for x in values]
    if len(values) == 1:
        values = values[0]

    parsed = values
    name = key

    match key:
        case "is_unknown":
            # Convert value to Boolean
            name = "unknown"
            parsed = values == "t"
        case "geolocation":
            name = "coords"
            parsed = {"lat": values.rsplit(",")[0], "lon": values.rsplit(",")[1]}
        case "_uri":
            name = "links"
        case "address":
            parsed = "".join(values)
        case "city":
            # temporary consider only first city
            parsed = values[0] if type(values) == list else values
        case "_ask":
            name = "slug"
            # it is a bit funny to undo the replace, but for the moment is ok.....
            parsed = values[0].replace(" ", "_")

    return name, parsed


def list_properties(
    category, base_url="https://www.securityvision.io/wiki/api.php", TEST=True
):
    SESSION = session.init(base_url)
    pages = queries.get_pages_for_category(category, base_url, SESSION, TEST)
    properties = set()
    for page in pages:
        data = queries.get_data_for_page(page, base_url, SESSION)
        if data["query"]["data"]:
            for el in data["query"]["data"]:
                properties.add(el["property"])
    return sorted(list(properties))
