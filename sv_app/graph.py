import networkx as nx
import numpy as np
from networkx.algorithms.centrality.betweenness import betweenness_centrality
from networkx.algorithms.centrality.eigenvector import eigenvector_centrality
from pprint import pprint

# from jo
# https://git.rubenvandeven.com/security_vision/securityvisionnr/src/commit/68bc2738998327fa4e8451cb006332a8f294ceb9/data-raw/wiki.R#L145

# mutate(connectivity = tidygraph::centrality_betweenness(),
# connectivity_normalised = 1 + (connectivity-min(connectivity))/sd(connectivity),
# size = connectivity_normalised * if_else(category == "deployments", 2, 1))


def make_graph(nodes, edges):
    G = nx.Graph()
    G.add_nodes_from([(node["id"], node) for node in nodes])
    G.add_edges_from([(edge["from"], edge["to"]) for edge in edges])
    return G


def louvain_communities(nodes, edges):
    G = make_graph(nodes, edges)
    communities = nx.community.louvain_communities(G, seed=123)
    return communities


def add_community_to_nodes(nodes, communities):
    """Adds a 'community' field to each node in the 'nodes' list based on the community IDs in the 'communities' list.

    Args:
      nodes: A list of dictionaries, where each dictionary represents a node.
      communities: A list of lists, where each inner list represents a community and contains the node IDs belonging to that community.

    Returns:
      A list of dictionaries, where each dictionary represents a node with an added 'community' field.
    """

    # Create a dictionary mapping node IDs to their corresponding community indices.
    id_to_community = {}
    for community_index, community in enumerate(communities):
        for node_id in community:
            id_to_community[node_id] = community_index

    # Add the 'community' field to each node based on the mapping.
    for node in nodes:
        node_id = node["id"]
        node["community"] = id_to_community[node_id]

    return nodes


def compute_betweenness(nodes, edges):
    G = make_graph(nodes, edges)

    bc = betweenness_centrality(G, k=10, normalized=True, weight=None)

    # Logarithmic scaling
    centrality = {k: np.log(v + 0.001) for k, v in bc.items()}

    # Min-Max scaling
    centrality = {
        k: (v - min(centrality.values()))
        / (max(centrality.values()) - min(centrality.values()))
        for k, v in centrality.items()
    }

    return [{"centrality": bc[node["id"]], **node} for node in nodes]


# https://networkx.org/documentation/latest/reference/algorithms/generated/networkx.algorithms.centrality.eigenvector_centrality.html#networkx.algorithms.centrality.eigenvector_centrality
def compute_eigenvector(nodes, edges):
    G = nx.Graph()
    G.add_nodes_from([(node["id"], node) for node in nodes])
    G.add_edges_from([(edge["from"], edge["to"]) for edge in edges])

    centrality = eigenvector_centrality(G, max_iter=1000)

    # Define percentiles for Winsorizing
    lower_percentile = 0.01
    upper_percentile = 0.9

    # Get the values at chosen percentiles (eigenvector centrality thresholds)
    winsor_threshold_low = np.percentile(list(centrality.values()), lower_percentile)
    winsor_threshold_high = np.percentile(list(centrality.values()), upper_percentile)

    # Apply Winsorizing (replace outliers with threshold values)
    winsorized_centrality = {
        k: (
            v
            if lower_percentile < v < upper_percentile
            else (
                winsor_threshold_low if v <= lower_percentile else winsor_threshold_high
            )
        )
        for k, v in centrality.items()
    }

    # Add a small constant to avoid log(0) errors
    constant = 0.001

    # Apply logarithmic scaling to winsorized centrality scores
    transformed_centrality = {
        k: np.log(v + constant) for k, v in winsorized_centrality.items()
    }

    tc_values = transformed_centrality.values()

    transformed_centrality = {
        k: (v - min(tc_values)) / (max(tc_values) - min(tc_values))
        for k, v in transformed_centrality.items()
    }

    return [
        {
            "centrality_e": transformed_centrality[node["id"]],
            **node,
        }
        for node in nodes
    ]


import json


def update_centrality():
    with open(f"instance/data.json", "r") as f:
        collection = json.load(f)

        collection["nodes"] = compute_betweenness(
            collection["nodes"], collection["links"]
        )

        collection["nodes"] = compute_eigenvector(
            collection["nodes"], collection["links"]
        )

    with open(f"instance/data.json", "w+") as f:
        json.dump(collection, f)
