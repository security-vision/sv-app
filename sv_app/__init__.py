import os
from flask import Flask
from dotenv import load_dotenv
from . import api
from . import docs
from . import utils


def create_app(test_config=None):
    load_dotenv()
    app = Flask(__name__, instance_relative_config=True)
    app.url_map.strict_slashes = False
    app.config.from_mapping(
        SECRET_KEY="dev",
        DATASET=os.path.join(app.instance_path, "dataset"),
        DEBUG=(os.getenv("DEBUG", "False") == "True"),
    )

    if test_config is None:
        app.config.from_pyfile("config.py", silent=True)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    app.register_blueprint(api.bp)
    app.register_blueprint(docs.bp)

    TEST = os.environ.get("TEST-DATA", "False") == "True"
    dt = utils.DataThread(app.instance_path, test=TEST)
    dt.start()

    return app
