import os
from . import utils
import json
from flask import (
    Blueprint,
    jsonify,
    request,
    redirect,
    url_for,
    send_from_directory,
    current_app,
)
from flask_cors import CORS
from . import graph

bp = Blueprint("api", __name__, url_prefix="/api/v1")
CORS(bp)


@bp.route("/data")
def get_data():
    return send_from_directory(
        current_app.instance_path, "data.json", mimetype="application/json"
    )


@bp.route("/update", methods=["GET", "POST"])
def update():
    if request.method == "POST":
        dt = utils.DataThread(current_app.instance_path)
        dt.start()
        return redirect(url_for("api.update"))

    return jsonify(
        json.loads(utils.last_line(os.path.join(current_app.instance_path, ".updates")))
    )


@bp.route("/properties/<category>")
def list_properties(category: str):
    propfile = os.path.join(current_app.instance_path, f"{category}.props.json")

    if request.method == "POST" or not os.path.exists(propfile):
        pt = utils.PropertiesThread(current_app.instance_path, category)
        pt.start()
        return redirect(url_for("api.list_properties", category=category))

    return send_from_directory(
        current_app.instance_path, f"{category}.props.json", mimetype="application/json"
    )
