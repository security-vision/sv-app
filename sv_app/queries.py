import json, requests, logging
from time import sleep

logger = logging.getLogger("queries")


def get_cities(url: str, session: requests.Session):
    """Return a list of cities with correspondent pair of coords"""

    city_query_params = {
        "action": "askargs",
        "conditions": "Category:City",
        "printouts": "Has Coordinates|Is in Country",
        "parameters": {},
        "format": "json",
    }

    cities = {}
    missing_coords = []

    while True:
        response = session.post(url, data=city_query_params)
        data = response.json()
        for key, value in data["query"]["results"].items():
            city = {"name": key, "url": value["fullurl"]}

            is_in_country = value["printouts"]["Is in Country"]
            if len(is_in_country) > 0:
                city["country"] = is_in_country[0]["fulltext"]

            has_coords = len(value["printouts"]["Has Coordinates"]) > 0
            if has_coords:
                city["coords"] = {
                    "lat": value["printouts"]["Has Coordinates"][0]["lat"],
                    "lon": value["printouts"]["Has Coordinates"][0]["lon"],
                }
            else:
                # Compile a list of cities with missing coords, useful to integrate data in the wiki
                missing_coords.append(city)
            cities[key] = city

        if "query-continue-offset" not in data:
            break

        city_query_params["parameters"] = f"offset={data['query-continue-offset']}"

    return cities, missing_coords


def get_countries(url: str, session: requests.Session):
    """
    Return a list of countries with correspondent pair of coords.
    Coords are selected using the available city connected to the country.
    """

    countries_query_params = {
        "action": "ask",
        "query": "[[Category:Country]] |?-Is in Country |?-Is in Country.Has Coordinates",
        "format": "json",
    }

    countries = {}
    missing_coords = []

    while True:
        response = session.post(url, data=countries_query_params)
        data = response.json()
        for key, value in data["query"]["results"].items():
            country = {"name": key}
            has_coords = len(value["printouts"]["Has Coordinates"]) > 0
            if has_coords:
                country["coords"] = {
                    "lat": value["printouts"]["Has Coordinates"][0]["lat"],
                    "lon": value["printouts"]["Has Coordinates"][0]["lon"],
                }
            else:
                # Compile a list of cities with missing coords, useful to integrate data in the wiki
                missing_coords.append(country)
            countries[key] = country

        if "query-continue-offset" not in data:
            break

        countries_query_params["query"] = (
            countries_query_params["query"] + f"|offset={data['query-continue-offset']}"
        )

    return countries, missing_coords


def get_pages_for_category(category, url: str, session: requests.Session, TEST=False):
    """Return a list of pages given a certain category"""

    logger.info(f'Getting pages for category: "{category}"')

    pages = []
    list_query_params = {
        "action": "query",
        "list": "categorymembers",
        "cmtitle": f"Category:{category}",
        "format": "json",
    }

    while True:
        # Repeat the query until there is a `continue` property (pagination)
        response = session.post(url, data=list_query_params)
        data = response.json()
        try:
            pages.extend(data["query"]["categorymembers"])
        except Exception as error:
            logger.error(data)
            raise error

        if TEST:
            break

        if "continue" not in data:
            break

        list_query_params["cmcontinue"] = data["continue"]["cmcontinue"]

    logger.info(f"Total pages: {len(pages)}")

    return pages


def get_data_for_page(page, url: str, session: requests.Session):
    """Return a list of properties for a specific page"""

    logger.info(f"Querying properties for page: {page['title']}")

    smwbrowse_query_params = {
        "action": "smwbrowse",
        "browse": "subject",
        "format": "json",
        "params": json.dumps({"subject": page["title"], "ns": page["ns"], "iw": ""}),
    }

    response = session.post(url, data=smwbrowse_query_params)
    data = response.json()

    return data
