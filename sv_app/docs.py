from flask import (
    Blueprint,
    render_template,
)

bp = Blueprint("docs", __name__, url_prefix="/")


@bp.route("/")
def docs():
    return render_template("docs.html")
