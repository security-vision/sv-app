# sv-data

Build a JSON dataset using the [securityvision.io/wiki](https://securityvision.io/wiki) API.

Work in progress workflow:

```
-> for each category
	-> get list of pages
	-> for each page
		-> get list of properties
		-> for each property
			-> parse property
		-> return parsed_properties
		-> for each parsed_property
			-> if link
				-> if target doesn't extists in targets
					-> create target and add to targets
				-> parse link
				-> add link to links
			-> else property
				-> add property to page
		add page to nodes
		add links to links
```

Output a file with the following structure:

```json
{
	"nodes": [
		{
			"id": 123,
			"name": "A page of the wiki",
			"some properties": "...other properties"
		}
	],
	"links": [
		{
			"source": 123,
			"target": 456,
			"name": "relation type"
		}
	]
}
```

## Setup

Install the app using poetry

```
poetry install
```

## App structure

-   A python script that connects to the wiki and build the dataset (`data.py`)
-   A simple API to interact with sv-data (trigger manual upates, get data) (`api.py`)
-   The dataviz app, developed with vite and vue. (at the moment `https://securityvision.io/viz/3d`)
-   A cron job that runs the script once a week or so, in order to keep the dataset in sync with the wiki

App structure:

-   The app starts and triggger the build of the dataset
-   If there are no problems with the build, the output overwrite the previous dataset
-   The building process caches its output as static file
-   If the process fail, the previous dataset is left untouched
-   The building process saves some details in a log file about last update

```
Endpoints:
- GET 	`/api/v1/data`			get dataset

- GET	`/api/v1/update`		get info about last update
- POST 	`/api/v1/update`		trigger a new build of the dataset
```

The dataset file is really big, so it could be a bit problematic to serve it from flask? So maybe flask could return a redirect to a static file served by Apache?

Also consider authentication for updates, in order to avoid excessive requests.
